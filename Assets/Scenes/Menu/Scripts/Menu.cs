using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NewGame(){
        SceneManager.LoadScene("Assets/Scenes/Menu/ChooseLevel.unity");
    }

    public void AuthorsScene()
	{
		SceneManager.LoadScene("Assets/Scenes/Menu/Authors.unity");
	}

    public void BackToMenu()
    {
		SceneManager.LoadScene("Assets/Scenes/Menu/Menu.unity");
	}

    public void ExitGame()
	{
		Application.Quit();
        //UnityEditor.EditorApplication.isPlaying = false;
	}

    public void onDestroy()
    {
        Destroy(GameObject.FindGameObjectWithTag("MenuMusic"));
    }
}


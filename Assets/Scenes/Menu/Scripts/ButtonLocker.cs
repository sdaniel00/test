using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ButtonLocker : MonoBehaviour
{

    [SerializeField]
    private LoadingLevel loadinglevels;
    bool flocked = true;

    // Start is called before the first frame update
    void Start()
    {
        flocked = loadinglevels.GetLevel(2);

        if(flocked == true)
        {
            GetComponent<Button>().interactable = false;
        }
        else
        {
            GetComponent<Button>().interactable = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingLevel : MonoBehaviour
{

    int unlocked_levels = 1;
    // Start is called before the first frame update
    void Awake()
    {

        LoadGame();
        Debug.Log(unlocked_levels);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadGame()
    {
        unlocked_levels = PlayerPrefs.GetInt("levels");
    }
    public bool GetLevel(int level){
      if(unlocked_levels >= level)
        return false;
      else
        return true;
    }

    public void Level1()
    {
        SceneManager.LoadScene(3);
        Destroy(GameObject.FindGameObjectWithTag("MenuMusic"));
    }
    public void Level2()
    {
        SceneManager.LoadScene(4);
        Destroy(GameObject.FindGameObjectWithTag("MenuMusic"));
    }

   
}

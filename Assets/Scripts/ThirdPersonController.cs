﻿using UnityEngine;
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
using UnityEngine.InputSystem;
#endif

/* Note: animations are called via the controller for both the character and capsule using animator null checks
 */

namespace StarterAssets
{

	public class ThirdPersonController : MonoBehaviour
	{

		public float MoveSpeed = 2.0f;

		[Range(0.0f, 0.3f)]
		public float RotationSmoothTime = 0.12f;

		public float SpeedChangeRate = 10.0f;

		public Item heldItem;
		public GameObject itemHelder;

		Vector3 cameraPosition;
		// player
		private float _speed;
		private float _animationBlend;
		private float _targetRotation = 0.0f;
		private float _rotationVelocity;
		private float _verticalVelocity;
		



		// animation IDs
		private int _animIDSpeed;

		private int _animIDMotionSpeed;

		private Animator _animator;
		private CharacterController _controller;
		private StarterAssetsInputs _input;
		private GameObject _mainCamera;
		RaycastHit hit;
		Interactable lastHit;


		private bool _hasAnimator;
		

		private void Awake()
		{
			// get a reference to our main camera
			if (_mainCamera == null)
			{
				_mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
			}
		}

		private void Start()
		{
			_hasAnimator = TryGetComponent(out _animator);
			_controller = GetComponent<CharacterController>();
			_input = GetComponent<StarterAssetsInputs>();

			AssignAnimationIDs();
			cameraPosition = _mainCamera.transform.position;
	

		}

		private void Update()
        {
            _hasAnimator = TryGetComponent(out _animator);

            _mainCamera.transform.position = transform.position + cameraPosition;
            //JumpAndGravity();
            //GroundedCheck();
            if (Input.GetKeyDown(KeyCode.Tab))
            {
				GameInstance.Instance.ShowInstruction();
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
				//GameInstance.Instance.Quit();
				GameInstance.Instance.LoadMainMenu();
			}
			HighlightOnView();
			Move();
            Interact();
        }

		
        private void HighlightOnView()
        {
            
            bool ray = Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 2.0f);
            if (ray)
            {
				
                var interactableHit = hit.transform.gameObject.GetComponent<Interactable>();

				
				if (interactableHit != null)
                {

					if (lastHit == null)
					{
						lastHit = interactableHit;
						lastHit.isHighlighted = true;

					}
					else if (lastHit != interactableHit)
                    {
						lastHit.isHighlighted = false;
						lastHit = null;
					}


                }
				else if (lastHit != null)
				{

					lastHit.isHighlighted = false;
					lastHit = null;

				}

			}
        }

        private void FixedUpdate()
        {
			
		}

        private void Interact()
        {
            
            // Does the ray intersect any objects excluding the player layer
            if (Input.GetKeyDown("space"))
            {
				//GameInstance.Instance.RobotRecived();
				bool ray = Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 2.0f);
				if(ray)
                {
					var interactableHit = hit.transform.gameObject.GetComponent<Interactable>();


						if (interactableHit != null)
						{

						
							var itemToGet = hit.transform.gameObject.GetComponent<ItemGiver>();
							var itemToGive = hit.transform.gameObject.GetComponent<ItemReciver>();
							var boxToGive = hit.transform.gameObject.GetComponent<RobotReciver>();

							if (itemToGet)
							{

								if (heldItem == null)
								{
									itemToGet.SpawnItem(itemHelder);
									heldItem = itemToGet.itemToGive;
								}
							}
							else if (itemToGive)
							{
                                if (itemToGive.isTrash)
                                {
								DestroyHeldItem();

							}
                                else
                                {
									if (heldItem != null && !heldItem.isContainer)
									{

										itemToGive.ReciveItme(heldItem);
									DestroyHeldItem();

								}
									else if (itemToGive.isItemCrafted() && heldItem.isContainer)
									{
										
										if (itemToGive.CraftedItem().isCompleated)
										{
										foreach (Transform child in itemHelder.transform)
										{
											foreach(Transform ch in child)
                                            {
												child.gameObject.SetActive(true);
											}
											
										}
										heldItem.SetContainsItem(itemToGive.CraftedItem());
											itemToGive.RemoveCraftedItem();
										}

									}
								}
	

							}else if (boxToGive)
                            {
                                if (boxToGive.canPacketBeRecived(heldItem) && heldItem.containsItem != null)
                                {
									
									boxToGive.RecivePacket(heldItem);

									boxToGive.AddToTotalRecivedRobots();
								DestroyHeldItem();
							}
                            }
                    }
                  

				}else if(heldItem != null)
                {
                    heldItem.PutDownItem(itemHelder);
                    DestroyHeldItem();
                }
						
                    
                }

            }
            
        

        public void DestroyHeldItem()
        {
            foreach (Transform child in itemHelder.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
			
			heldItem = null;
        }

        private void AssignAnimationIDs()
		{
			_animIDSpeed = Animator.StringToHash("Speed");


			_animIDMotionSpeed = Animator.StringToHash("MotionSpeed");
		}




		private void Move()
		{
			// set target speed based on move speed, sprint speed and if sprint is pressed
			float targetSpeed = MoveSpeed;

			// a simplistic acceleration and deceleration designed to be easy to remove, replace, or iterate upon

			// note: Vector2's == operator uses approximation so is not floating point error prone, and is cheaper than magnitude
			// if there is no input, set the target speed to 0
			if (_input.move == Vector2.zero) targetSpeed = 0.0f;

			// a reference to the players current horizontal velocity
			float currentHorizontalSpeed = new Vector3(_controller.velocity.x, 0.0f, _controller.velocity.z).magnitude;

			float speedOffset = 0.1f;
			float inputMagnitude = _input.analogMovement ? _input.move.magnitude : 1f;

			// accelerate or decelerate to target speed
			if (currentHorizontalSpeed < targetSpeed - speedOffset || currentHorizontalSpeed > targetSpeed + speedOffset)
			{
				// creates curved result rather than a linear one giving a more organic speed change
				// note T in Lerp is clamped, so we don't need to clamp our speed
				_speed = Mathf.Lerp(currentHorizontalSpeed, targetSpeed * inputMagnitude, Time.deltaTime * SpeedChangeRate);

				// round speed to 3 decimal places
				_speed = Mathf.Round(_speed * 1000f) / 1000f;
			}
			else
			{
				_speed = targetSpeed;
			}
			_animationBlend = Mathf.Lerp(_animationBlend, targetSpeed, Time.deltaTime * SpeedChangeRate);

			// normalise input direction
			Vector3 inputDirection = new Vector3(_input.move.x, 0.0f, _input.move.y).normalized;

			// note: Vector2's != operator uses approximation so is not floating point error prone, and is cheaper than magnitude
			// if there is a move input rotate player when the player is moving
			if (_input.move != Vector2.zero)
			{
				_targetRotation = Mathf.Atan2(inputDirection.x, inputDirection.z) * Mathf.Rad2Deg;
				float rotation = Mathf.SmoothDampAngle(transform.eulerAngles.y, _targetRotation, ref _rotationVelocity, RotationSmoothTime);

				// rotate to face input direction relative to camera position
				transform.rotation = Quaternion.Euler(0.0f, rotation, 0.0f);
			}


			Vector3 targetDirection = Quaternion.Euler(0.0f, _targetRotation, 0.0f) * Vector3.forward;

			// move the player
			_controller.Move(targetDirection.normalized * (_speed * Time.deltaTime) + new Vector3(0.0f, _verticalVelocity, 0.0f) * Time.deltaTime);

			// update animator if using character
			if (_hasAnimator)
			{
				_animator.SetFloat(_animIDSpeed, _animationBlend);
				_animator.SetFloat(_animIDMotionSpeed, inputMagnitude);
			}
		}




	}
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMusic : MonoBehaviour
{
    [SerializeField]
    AudioClip []music;
    int i = 0;

    // Start is called before the first frame update

    
    void Awake()
    {
        GetComponent<AudioSource>().clip = music[0];
        GetComponent<AudioSource>().Play();
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if(GetComponent<AudioSource>().isPlaying == false)
        {
            i++;
            GetComponent<AudioSource>().clip = music[i];
            GetComponent<AudioSource>().Play();
        }
    }
}

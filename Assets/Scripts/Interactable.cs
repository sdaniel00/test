using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public bool isHighlighted = false;
    bool wasHighlighted = true;
    MeshRenderer renderer = new MeshRenderer();

    private void Awake()
    {
        renderer = GetComponent<MeshRenderer>();
    }
    
    public void Highlight()
    {
        
        

        if (wasHighlighted)
        {
            int i = 0;
            foreach (Material m in renderer.materials)
            {
                
                renderer.materials[i].SetColor("_EmissionColor", new Color(0.5f, 0.5f, 0.5f, 1));

                i++;
            }
            
            
            
        }
        else if(!wasHighlighted) {
            int i = 0;
            foreach (Material m in renderer.materials)
            {
                
                renderer.materials[i].SetColor("_EmissionColor", new Color(0.0f, 0.0f, 0.0f, 1));

                i++;
            }
            
        }
           

    }

    private void Update()
    {
        
        if (isHighlighted)
        {
            if (!wasHighlighted)
            {
                wasHighlighted = true;
            }
            Highlight();
        }
        else if(!isHighlighted)
        {
            if (wasHighlighted)
            {
                wasHighlighted = false;
            }
            Highlight();
        }
    }


}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameInstance : MonoBehaviour
{

    public static GameInstance Instance => gameInstance;
    private static GameInstance gameInstance;

    [Header("Time")]
    public float time;
    public float maxTime;
    public Slider timeSlider;
    [Header("UI")]
    public GameObject endLevelPanel;
    public Image[] stars;
    public TextMeshProUGUI endLevelText;
    public Button nextLevel;
    public Sprite trybik;
    public Image[] inGameStars;
    public GameObject instruction;
    public TextMeshProUGUI text;
    public GameObject quit;
    [Header("Robots")]
    public Item[] robotsToRecive;
    public int[] robotNumbrRequired;
    private int[] currentRobotsDelivered;

    bool victory = false;
    bool defeat = false;
    int maxStars = 5;
    int starsRecived = 0;
    int maxNumberOfCraftedRobots = 0;
    int numberOfRecivedRobots = 0;
    int it = 0;
    void Start()
    {
        gameInstance = this;
        currentRobotsDelivered = new int[robotNumbrRequired.Length];
        foreach(int i in robotNumbrRequired)
        {
            maxNumberOfCraftedRobots += i;
        }

        foreach(Item i in robotsToRecive)
        {
            text.text += "\n " + i.name+ ": x " + robotNumbrRequired[it];
            it++;
        }
        
        
    }

    
    void Update()
    {
        time += Time.deltaTime;

        if (!victory && !defeat)
        {
            Victory();
            Defeat();
            if(victory || defeat)
            {
                DisplayEndLevelPanel();
            }
            
        }
        
        
    }
    private void FixedUpdate()
    {
        UpdateSlider();
    }
    public void UpdateSlider()
    {
        timeSlider.value = time / maxTime;
    }
    public void RobotRecived()
    {
        numberOfRecivedRobots++;
        SetStarsRecived();
        FillInGameStars();

      
    }

    public void IncrementCurrentRobotNumbers(int i)
    {
        it = 0;
        currentRobotsDelivered[i]++;
        text.text = "";
        foreach (Item ite in robotsToRecive)
        {
            text.text += "\n " + ite.name + ": x " + (robotNumbrRequired[it] -  currentRobotsDelivered[it]);
            it++;
        }
    }

    public int GetCurrnetRobotsDelivered(int i)
    {
        return currentRobotsDelivered[i];
    }
    public void Victory()
    {
        if (numberOfRecivedRobots > 0 && time >= maxTime || numberOfRecivedRobots >= maxNumberOfCraftedRobots)
        {
            victory = true;
            
        }
    }
    public void Defeat()
    {
        if(numberOfRecivedRobots == 0 && time >= maxTime)
        {
            defeat = true;
            
        }
    }
    void DisplayEndLevelPanel()
    {
        if (victory)
        {
            endLevelText.text = "Victory";
            PlayerPrefs.SetInt("levels", 2);
        }
        if(defeat)
        {
            endLevelText.text = "Defeat";
            nextLevel.gameObject.SetActive(false);
        }
        endLevelPanel.SetActive(true);
        SetStarsRecived();
        Cursor.visible = true;
        StartCoroutine(FillStars());
        

    }

    IEnumerator FillStars()
    {
        float elapsedTime = 0;
        int i = 0;
        while (i < starsRecived)
        {

            elapsedTime += Time.deltaTime;
            if (elapsedTime > 0.4)
            {
                stars[i].sprite = trybik;
                i++;
                elapsedTime = 0;
            }
            
            yield return null;
        }
    }
    void FillInGameStars()
    {
        if (starsRecived > 0)
        {
            for(int i = 0; i < starsRecived; i++)
            {
                inGameStars[i].sprite = trybik;
            }
        }
    }
    void SetStarsRecived()
    {
        float percent;
       percent = numberOfRecivedRobots / (float)maxNumberOfCraftedRobots;
        starsRecived = (int)(maxStars * percent);
        
    }
    
    public void ShowInstruction()
    {
        if (instruction.activeSelf)
        {
            instruction.SetActive(false);
        }
        else
        {
            instruction.SetActive(true);
        }
    }
    public void Quit()
    {
        if (quit.activeSelf)
        {
            quit.SetActive(false);
        }
        else
        {
            quit.SetActive(true);
        }
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene(0);
    }
}

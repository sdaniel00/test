using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemGiver : Interactable
{
    public Item itemToGive;
    public bool isItem;
    public Image image;

    

    public void SpawnItem(GameObject helder)
    {
        
        var spawnedItem = Instantiate(itemToGive, helder.transform.position, itemToGive.transform.rotation);
        if (itemToGive.isContainer)
        {
            itemToGive.transform.GetChild(0).gameObject.SetActive(false);
            itemToGive.containsItem = null;
        }
        spawnedItem.transform.parent = helder.transform;
        if (isItem)
        {
            Destroy(gameObject);
        }
    } 


}

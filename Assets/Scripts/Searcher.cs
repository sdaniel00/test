using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Searcher : MonoBehaviour
{


    // Start is called before the first frame update
    [ SerializeField ]
    private GameObject item1;
    [ SerializeField ]
    private GameObject item2;
    [ SerializeField ]
    private GameObject item3;
    [ SerializeField ]
    private GameObject item4;
    [ SerializeField ]
    private GameObject workshop1;
    [ SerializeField ]
    private GameObject workshop2;
    [ SerializeField ]  
    private GameObject itemHelder;

    private UnityEngine.AI.NavMeshAgent agent;
    private Item heldItem;
    private Vector3 waypoint;
       
    void Start(){
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        waypoint = new Vector3(-41.95f,0,-4.45f);
        InvokeRepeating("Search",2,5);
    }

    // Update is called once per frame
    void Update(){
        if(Vector3.Distance(transform.position,waypoint) < 1.5){
            foreach (Transform child in itemHelder.transform)
			{
				GameObject.Destroy(child.gameObject);
			}
			heldItem = null;
        }
    }
    
    
    private void OnTriggerEnter(Collider other) {  
			if (other.transform.gameObject.GetComponentInParent<Interactable>() != null)
			{
                Debug.Log("Kolizja " + other);
				var itemToGet = other.GetComponentInParent<ItemGiver>();
				var itemToGive = other.GetComponentInParent<ItemReciver>();
				if (itemToGet)
				{	
					if (heldItem == null)
					{
				    	itemToGet.SpawnItem(itemHelder);
						heldItem = itemToGet.itemToGive;
                        agent.destination = waypoint;
					}
				}
				else if (itemToGive)
				{
					if (heldItem != null)
					{
						Debug.Log(heldItem.name);
						itemToGive.ReciveItme(heldItem);
						foreach (Transform child in itemHelder.transform)
						{
							GameObject.Destroy(child.gameObject);
						}
						heldItem = null;
                              
					}else if (itemToGive.isItemCrafted())
					{
						heldItem = itemToGive.CraftedItem();
						itemToGive.GiveCraftedItem(itemHelder);
					}
				}
			}
    }
    

    void Search(){
        if(heldItem == null){
            int randomizer = Random.Range(1,3);
            switch(randomizer){

                case 1:
                    agent.destination = item1.GetComponent<Transform>().position; 
                    break;
                case 2:
                    agent.destination = item2.GetComponent<Transform>().position; 
                    break;
                case 3:
                    agent.destination = item3.GetComponent<Transform>().position; 
                    break;
                case 4:
                    agent.destination = item4.GetComponent<Transform>().position;
                    break;
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platforme : MonoBehaviour
{
    // Start is called before the first frame update
    float actualRotation = 0;
    bool isRotating = false;
    bool isOnPosition = true;
    void Start()
    {
        InvokeRepeating("setNewPosition",2,4);
    }

    // Update is called once per frame
    void Update()
    {
        if(isRotating == true){
            if(isOnPosition == false){
                transform.Rotate(0.0f,0.1f,0.0f);
                actualRotation += 0.1f;
                if(actualRotation == 90) {
                    actualRotation = 0;
                    isOnPosition = true;
                }
            }
            else{
                isRotating = false;
            }
        }    
    }

    void setNewPosition(){
        Debug.Log("Zaczynam");
        StartCoroutine(ExampleCoroutine());
    }

    IEnumerator ExampleCoroutine()
    {
        //Print the time of when the function is first called.
        iTween.RotateAdd(gameObject,iTween.Hash("z", 90.0f, "time", 3.0f, "easetype" ,"linear"));

        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(1.0f);

        //After we have waited 5 seconds print the time again.
        Debug.Log("Finished Coroutine at timestamp : " + Time.time);
    }
}

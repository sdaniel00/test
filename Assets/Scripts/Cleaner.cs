using UnityEngine;
using System.Collections;
 
 public class Cleaner : MonoBehaviour
 {
     [SerializeField]
     float Speed = 1;
 
     Vector3 wayPoint;
 
     [SerializeField]
     int maxRange = 20;
     [SerializeField]
     int minRange = 5;

    float timer = 10;
    bool ftimer = false;
 
     void Start()
     {
         transform.eulerAngles = new Vector3(
            -90,
            0,
            0
        );
         //initialise the target way point
         wander();
     }
 
     void Update()
     {

         if(ftimer == true){
            timer -= Time.deltaTime;
            if(timer < 0 )
            {
                ftimer = false;
                wander();
            }
         }
         // this is called every frame
         // do move code here
        transform.Translate(transform.forward * Time.deltaTime * Speed,Space.World);
        transform.position = new Vector3(transform.position.x,0.12f,transform.position.z);

         if ((transform.position - wayPoint).magnitude < 3)
        {
             // when the distance between us and the target is less than 3
             // create a new way point target
             wander();
        }
     }
 
    void wander()
    {
        do
        {
            wayPoint.x = Random.Range(transform.position.x - maxRange, transform.position.x + maxRange);
        }while(wayPoint.x >= transform.position.x - minRange && wayPoint.x <= transform.position.x + minRange);

        do
        {
            wayPoint.z = Random.Range(transform.position.z - maxRange, transform.position.z + maxRange);
        }while(wayPoint.z >= transform.position.z - minRange && wayPoint.z <= transform.position.z + minRange);


        transform.LookAt(wayPoint);
         
    }

    private void OnCollisionEnter(Collision other) {
        timer = 10;
        ftimer = true;

        transform.RotateAround(transform.position, Vector3.up, 90);
    }
}

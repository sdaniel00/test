using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemReciver : Interactable
{

    public bool isTrash = false;
    bool isWorking = false;
    bool canRecive = false;
    float workTime;
    float destroyTime;
    public List<Item> recivedItems;
    public Recipt[] recipts;
    private Item createdItem;
    public GameObject placeItem;

    public GameObject itemPanel;
    public List<Image> itemSprites;
    public GameObject workPanel;
    public GameObject warrningPanel;
    public GameObject destroyedPanel;
    [SerializeField]
    AudioClip[] clips;

    int i = 0;


    public void ReciveItme(Item item)
    {
        if (!isWorking)
        {
            if (!itemPanel.activeSelf)
            {
                DisableAllPanels();
                itemPanel.SetActive(true);
            }

            if (!itemSprites[i].gameObject.activeSelf)
            {
                itemSprites[i].sprite = item.itemIcon;
                itemSprites[i].gameObject.SetActive(true);
            }
            i++;

            if(item != null)
            {
                recivedItems.Add(item);
            }
               
            
                
                foreach (Recipt rc in recipts)
                {
                    if (CompareLists<Item>(rc.itemList, recivedItems))
                {
                    workTime = rc.creationTime;
                    destroyTime = workTime;
                    ClearList();
                    GetComponent<AudioSource>().clip = clips[1];
                    GetComponent<AudioSource>().Play();
                    StartCoroutine(Work(rc));



                }
                if (isWorking)
                    {
                        break;
                    }
                }
            if (recivedItems.Count >= 4)
            {
                Debug.Log("Za duzo");
                recivedItems = new List<Item>();
                i = 0;
               StartCoroutine( DestroyPanelWait());
            }



        }
    }

    private void ClearList()
    {
        recivedItems = new List<Item>();
        foreach (Image i in itemSprites)
        {
            i.sprite = null;
            i.gameObject.SetActive(false);
        }
    }

    void SpawnCraftedItem()
    {
        GetComponent<AudioSource>().clip = clips[0];
        GetComponent<AudioSource>().Play();
        var created = Instantiate(createdItem, placeItem.transform.position + new Vector3(0, 1, 0), Quaternion.identity);

        created.transform.parent = placeItem.transform;
    }

    IEnumerator Work(Recipt rc)
    {
        i = 0;
        DisableAllPanels();
        workPanel.SetActive(true);
        isWorking = true;

        
        yield return new WaitForSeconds(workTime);
        
        createdItem = rc.reciptItem;
        SpawnCraftedItem();
        canRecive = true;
        isWorking = false;
        DisableAllPanels();
        StartCoroutine(Destroying());


    }
    IEnumerator Destroying()
    {
       
        warrningPanel.SetActive(true);
        yield return new WaitForSeconds(destroyTime);
        DisableAllPanels();
        if (createdItem != null)
        {
            
            StartCoroutine(DestroyPanelWait());
            
        }
        else
        {
            itemPanel.SetActive(true);
        }
        
        


    }

    IEnumerator DestroyPanelWait()
    {
        ClearList();
        foreach (Transform child in placeItem.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        createdItem = null;

        GetComponent<AudioSource>().clip = clips[2];
        GetComponent<AudioSource>().Play();

        DisableAllPanels();
        destroyedPanel.SetActive(true);
        yield return new WaitForSeconds(2.0f);
        DisableAllPanels();
        itemPanel.SetActive(true);
    }

    public bool isItemCrafted()
    {
        if (createdItem != null)
        {
            return true;
        }
        else
            return false;
    }

    public Item CraftedItem()
    {
        return createdItem;
    }

    public static bool CompareLists<T>(List<T> aListA, List<T> aListB)
    {
        if (aListA == null || aListB == null || aListA.Count != aListB.Count)
            return false;
        if (aListA.Count == 0)
            return true;
        Dictionary<T, int> lookUp = new Dictionary<T, int>();
        // create index for the first list
        for (int i = 0; i < aListA.Count; i++)
        {
            int count = 0;
            if (!lookUp.TryGetValue(aListA[i], out count))
            {
                lookUp.Add(aListA[i], 1);
                continue;
            }
            lookUp[aListA[i]] = count + 1;
        }
        for (int i = 0; i < aListB.Count; i++)
        {
            int count = 0;
            if (!lookUp.TryGetValue(aListB[i], out count))
            {
                // early exit as the current value in B doesn't exist in the lookUp (and not in ListA)
                return false;
            }
            count--;
            if (count <= 0)
                lookUp.Remove(aListB[i]);
            else
                lookUp[aListB[i]] = count;
        }
        // if there are remaining elements in the lookUp, that means ListA contains elements that do not exist in ListB
        return lookUp.Count == 0;
    }

    public void RemoveCraftedItem()
    {
        itemPanel.SetActive(true);
        foreach (Transform child in placeItem.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        
        createdItem = null;

        canRecive = false;
 
    }

    public void DisableAllPanels()
    {
        destroyedPanel.SetActive(false);
        itemPanel.SetActive(false);
        warrningPanel.SetActive(false);
        workPanel.SetActive(false);
    }

    public void GiveCraftedItem(GameObject itemHelder)
    {

    }
}

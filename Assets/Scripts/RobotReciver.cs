using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotReciver : Interactable
{
    Item recivedItem;
    public GameObject robotInPacket;
    Vector3 robotPosition;

    

    private void Start()
    {
        robotPosition = robotInPacket.transform.position;
        
    }

    public void RecivePacket(Item item)
    {
        recivedItem = item;
        PacketRecived();
    }
    public void PacketRecived()
    {
        var robot = Instantiate(recivedItem, robotPosition, recivedItem.transform.rotation);
        robot.transform.parent = robotInPacket.transform;
        StartCoroutine(MoveObject());
    }

    IEnumerator MoveObject()
    {
        Vector3 startingPos = robotInPacket.transform.position;
        Vector3 finalPos = startingPos + (transform.up * 5);
        float elapsedTime = 0;

        while (elapsedTime < 6)
        {
            robotInPacket.transform.position = Vector3.Lerp(startingPos, finalPos, (elapsedTime / 5));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        

        foreach (Transform child in robotInPacket.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        robotInPacket.transform.position = robotPosition;
    }

    public bool canPacketBeRecived(Item item)
    {
        return item != null && item.isContainer && (item.GetContainedItem() != null);
    }

    public void AddToTotalRecivedRobots()
    {
        
        int ix = 0;
        Debug.Log(GameInstance.Instance.robotsToRecive[0]);
        Debug.Log("1");
        foreach (Item i in GameInstance.Instance.robotsToRecive)
        {
            
            if(i.Equals(recivedItem.containsItem) && GameInstance.Instance.GetCurrnetRobotsDelivered(ix) <= GameInstance.Instance.robotNumbrRequired[ix])
            {
                GameInstance.Instance.RobotRecived();
                GameInstance.Instance.IncrementCurrentRobotNumbers(ix);
                break;
            }
            ix++;
        }
    }
}

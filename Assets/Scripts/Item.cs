using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public Sprite itemIcon;
    public GameObject itemPref;
    public bool isCompleated = false;
    public bool isContainer = false;
    public Item containsItem = null;
    public float distanceFromPlayer;
    StarterAssets.ThirdPersonController player;
    bool canPickUp;

    private void Start()
    {
        player = FindObjectOfType<StarterAssets.ThirdPersonController>();
    }
    private void Update()
    {
        distanceFromPlayer = Vector3.Distance(transform.position, player.transform.position);
        if(distanceFromPlayer < 1 && !canPickUp)
        {
            canPickUp = true;
        }else if (canPickUp)
        {
            canPickUp = false;
        }
        

    }
    public Item GetContainedItem()
    {
        if (containsItem != null)
            return containsItem;
        else
            return null;
    }
    public void SetContainsItem(Item item)
    {
        foreach(Transform child in this.transform)
        {
            child.gameObject.SetActive(true);
        }
        containsItem = item;
    }

    public void PutDownItem(GameObject position)
    {
        Instantiate(itemPref, position.transform.position + new Vector3(0, -1, 0),itemPref.transform.rotation);
        if(containsItem != null)
        {
            itemPref.GetComponent<ItemGiver>().itemToGive.containsItem = containsItem;
        }
        if (isContainer)
        {
            foreach (Transform child in itemPref.transform)
            {
                child.gameObject.SetActive(true);
            }
        }
        
 
    }
 
    
    public bool GetCanPickUp()
    {
        return canPickUp;
    }
   
}
